from setuptools import setup

from sigmatheta.version import __version__


setup(name='sigmatheta',
      version=__version__,
      scripts=['bin/SigmaTheta-gui',
               'bin/SigmaTheta'],
      packages=['sigmatheta',
                'sigmatheta.core',
                'sigmatheta.ui',
                'sigmatheta.data'],
      package_data={'sigmatheta.data': ["data/*.png"]},
      install_requires=['st-binding',
                        'numpy',
                        'scipy',
                        'pyqtgraph',
                        'PyQt5'],
      download_url=['https://gitlab.com/bendub/sigmatheta/blob/master/dist/sigmatheta-0.1.0.tar.gz'],
      author='Benoit Dubois',
      author_email='dubois.benoit@gmail.com',
      description='GUI for Sigma Theta library',
      classifiers=[
          'Development Status :: 3 - Alpha',
          'Environment :: Console',
          'Environment :: X11 Applications :: Qt',
          'Intended Audience :: End Users/Desktop',
          'License :: OSI Approved :: GNU General Public License (GPL) v3 or later (GPLv3+)',
          'Natural Language :: English',
          'Operating System :: POSIX',
          'Programming Language :: Python',
          'Topic :: Scientific/Engineering'],
      include_package_data=True,
      zip_safe=False)
