# -*- coding: utf-8 -*-

"""package sigmatheta
author    Benoit Dubois
copyright Benoit Dubois, 2022
email     dubois.benoit@gmail.com
licence   GPL3+
brief     UI part of Sigma Theta program.
"""

import logging
import ntpath
import numpy as np
from PyQt5 import Qt, QtCore, QtGui, QtWidgets
import pyqtgraph as pg
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter, ParameterTree, ParameterItem, \
    registerParameterType
import importlib.resources as impr
import st_binding.st_binding as stb
from sigmatheta.constants import APP_NAME
import sigmatheta.data as data


## Use white background and black foreground
pg.setConfigOption('background', 'w')
pg.setConfigOption('foreground', 'k')
# Enable antialiasing for prettier plots
pg.setConfigOptions(antialias=True)


LEGEND_FORMAT = ':.3'

param_dict = [
    {'name': 'Temporal analysis', 'type': 'group', 'children': [
        {'name': 'Filename', 'type': 'str', 'value': '', 'readonly': True},
        {'name': 'Deviance type', 'type': 'list',
         'values': list(stb.DEVIANCE_TYPE.keys()),
         'value': list(stb.DEVIANCE_TYPE)[0]},
        {'name': 'Integration time (s)', 'type': 'int', 'value': 1.0},
        {'name': 'Tau step', 'type': 'list',
         'values': {"Decade": 2, "All tau": 3}, 'value': 2}, # Octave not implemented yet
        #{'name': 'Tau step', 'type': 'list',
        # 'values': {"Octave": 1, "Decade": 2, "All tau": 3}, 'value': 1},
        {'name': 'Number of points', 'type': 'int',
         'value': 0, 'readonly': True},
        {'name': 'Start', 'type': 'int', 'limits': (1, 1),'value': 1},
        {'name': 'Stop', 'type': 'int', 'limits': (1, 1),'value': 1},
        {'name': 'Display asymptote', 'type': 'bool', 'value': False}
    ]}
]

periodogram_params = [
    {'name': 'fs', 'type': 'float', 'value': 1.0},
    {'name': 'window',
     'type': 'list',
     'values': ['boxcar',
                'triang',
                'blackman',
                'hamming',
                'hann',
                'hanning',
                'bartlett',
                'flattop',
                'parzen',
                'bohman',
                'blackmanharris'],
     'value': 'boxcar'},
    {'name': 'nfft', 'type': 'int', 'limits': (1, 32768), 'value': None},
    #{'name': 'detrend'},
    #{'name': 'return_onesided', 'type': 'bool', 'value': rue},
    {'name': 'scaling', 'type': 'list',
     'values': ['density', 'spectrum'], 'value': 'density'}
]

welch_params = [
    {'name': 'fs', 'type': 'float', 'value': 1.0},
    {'name': 'window', 'type': 'list', 'values':['boxcar', 'triang', 'blackman', 'hamming', 'hann', 'hanning', 'bartlett', 'flattop', 'parzen', 'bohman', 'blackmanharris'], 'value': 'hanning'},
    {'name': 'nperseg', 'type': 'int', 'limits': (1, 32768), 'value': 256},
    {'name': 'noverlap', 'type': 'int', 'limits': (1, 32768), 'value': None},
    {'name': 'nfft', 'type': 'int', 'limits': (1, 32768), 'value':None},
    #{'name': 'detrend'},
    #{'name': 'return_onesided', 'type': 'bool', 'value': True},
    {'name': 'scaling', 'type': 'list', 'values': ['density', 'spectrum'], 'value': 'density'}
]

psd_algorithm = ['Periodogram', 'Welch']

psd_algo_params = [periodogram_params, welch_params]

psd_params = [
    {'name': 'Frequency analysis', 'type': 'group', 'children': [
        {'name': 'Algorithm', 'type': 'list', 'values': psd_algorithm, 'value': 0},
        {'name': 'Frequency analysis', 'type': 'group', 'children':[
            psd_algo_params[0]
        ]},
        {'name': 'Process analysis', 'type': 'action'},
    ]}
]

# =============================================================================
class PsdDialog(QtWidgets.QDialog):
    """PsdDialog class, generate a dialog box used to get back PSD parameters.
    """

    def __init__(self, parent=None):
        """Constructor.
        :param parent: parent of widget (object)
        :returns: None
        """
        super().__init__(parent=parent)
        self.setWindowTitle("PSD")
        # Lays out
        self._drift_order_led = QtWidgets.QLineEdit()
        self._drift_order_led.setValidator(QtGui.QIntValidator(
            self._drift_order_led))
        self._drift_order_led.setText(str(self.DEFAULT_DRIFT_REMOVE_ORDER))
        self._btn_box = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok |
            QtWidgets.QDialogButtonBox.Cancel)
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(QtWidgets.QLabel("Drift degree"))
        layout.addWidget(self._drift_order_led)
        layout.addWidget(self._btn_box)
        self.setLayout(layout)
        # Basic logic
        self._btn_box.accepted.connect(self.accept)
        self._btn_box.rejected.connect(self.close)

    @property
    def drift_order(self):
        """Getter of the drift order value.
        :returns: drift order value (int)
        """
        return int(self._drift_order_led.text())

# =============================================================================
class DriRemDialog(QtWidgets.QDialog):
    """DriRemDialog class, generate a dialog box used to get back drift remove
    parameters to be applied to a curve.
    """

    # TODO: add preview

    DEFAULT_DRIFT_REMOVE_ORDER = 1

    def __init__(self, parent=None):
        """Constructor.
        :param parent: parent of widget (object)
        :returns: None
        """
        super().__init__(parent=parent)
        self.setWindowTitle("Drift remove scaling")
        # Lays out
        self._drift_order_led = QtWidgets.QLineEdit()
        self._drift_order_led.setValidator(QtGui.QIntValidator(
            self._drift_order_led))
        self._drift_order_led.setText(str(self.DEFAULT_DRIFT_REMOVE_ORDER))
        self._btn_box = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok |
            QtWidgets.QDialogButtonBox.Cancel)
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(QtWidgets.QLabel("Drift degree"))
        layout.addWidget(self._drift_order_led)
        layout.addWidget(self._btn_box)
        self.setLayout(layout)
        # Basic logic
        self._btn_box.accepted.connect(self.accept)
        self._btn_box.rejected.connect(self.close)

    @property
    def drift_order(self):
        """Getter of the drift order value.
        :returns: drift order value (int)
        """
        return int(self._drift_order_led.text())


# =============================================================================
class X2yDialog(QtWidgets.QDialog):
    """X2yDialog class, generate a dialog box used to get back scaling
    parameters to be applied to a time to frequency conversion.
    """

    def __init__(self, parent=None):
        """Constructor.
        :param parent: parent of widget (object)
        :returns: None
        """
        super().__init__(parent=parent)
        self.setWindowTitle("X2Y conversion")
        # Lays out
        self._xscale_led = QtWidgets.QLineEdit()
        self._xscale_led.setValidator(QtGui.QDoubleValidator(self._xscale_led))
        self._xscale_led.setText(str(1.0))
        self._yscale_led = QtWidgets.QLineEdit()
        self._yscale_led.setValidator(
            QtGui.QDoubleValidator(self._yscale_led))
        self._yscale_led.setText(str(1.0))
        self._btn_box = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok |
            QtWidgets.QDialogButtonBox.Cancel)
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(QtWidgets.QLabel("Timetag factor"))
        layout.addWidget(self._xscale_led)
        layout.addWidget(QtWidgets.QLabel("Phase factor"))
        layout.addWidget(self._yscale_led)
        layout.addWidget(self._btn_box)
        self.setLayout(layout)
        # Basic logic
        self._btn_box.accepted.connect(self.accept)
        self._btn_box.rejected.connect(self.close)

    @property
    def xscale(self):
        """Getter of the xscale factor value.
        :returns: xscale factor value (float)
        """
        return float(self._xscale_led.text())

    @property
    def yscale(self):
        """Getter of the yscale value.
        :returns: yscale value (float)
        """
        return float(self._yscale_led.text())


# =============================================================================
class ScalingDialog(QtWidgets.QDialog):
    """ScalingDialog class, generate a dialog box used to get back scaling
    parameters to be applied to a curve.
    """

    def __init__(self, parent=None):
        """Constructor.
        :param parent: parent of widget (object)
        :returns: None
        """
        super().__init__(parent=parent)
        self.setWindowTitle("Data scaling")
        # Lays out
        self._scale_led = QtWidgets.QLineEdit()
        self._scale_led.setValidator(QtGui.QDoubleValidator(self._scale_led))
        self._scale_led.setText(str(1.0))
        self._offset_led = QtWidgets.QLineEdit()
        self._offset_led.setValidator(
            QtGui.QDoubleValidator(self._offset_led))
        self._offset_led.setText(str(0.0))
        self._btn_box = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok |
            QtWidgets.QDialogButtonBox.Cancel)
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(QtWidgets.QLabel("Multiplier factor"))
        layout.addWidget(self._scale_led)
        layout.addWidget(QtWidgets.QLabel("Sum factor"))
        layout.addWidget(self._offset_led)
        layout.addWidget(self._btn_box)
        self.setLayout(layout)
        # Basic logic
        self._btn_box.accepted.connect(self.accept)
        self._btn_box.rejected.connect(self.close)

    @property
    def scale(self):
        """Getter of the scale factor value.
        :returns: scale factor value (float)
        """
        return float(self._scale_led.text())

    @property
    def offset(self):
        """Getter of the offset value.
        :returns: offset value (float)
        """
        return float(self._offset_led.text())


# =============================================================================
class MyErrorBarItem(pg.ErrorBarItem):
    """Subclass ErrorBarItem to add log mode display of error bar.
    """

    def __init__(self, **opts):
        """Constructor.
        """
        super().__init__(**opts)

    def setLogMode(self, x, y):
        """ This will be called by the PlotItem when the error bar is first
        added and whenever the log scaling mode changes.
        """
        if x is True:
            self.logx = True
        if y is True:
            self.logy = True

    def drawPath(self):
        p = pg.QtGui.QPainterPath()
        x, y = self.opts['x'], self.opts['y']
        if x is None or y is None:
            return
        beam = self.opts['beam']
        height, top, bottom = self.opts['height'], \
                              self.opts['top'], \
                              self.opts['bottom']
        if self.logx is True:
            x = np.log10(x)
        if self.logy is True:
            y = np.log10(y)
        if height is not None or top is not None or bottom is not None:
            ## draw vertical error bars
            if height is not None:
                if self.logy is True:
                    y1 = np.log10(10**y - height/2.)
                    y2 = np.log10(10**y + height/2.)
                else:
                    y1 = y - height/2.
                    y2 = y + height/2.
            else:
                if bottom is None:
                    y1 = y
                else:
                    if self.logy is True:
                        y1 = np.log10(10**y - bottom)
                    else:
                        y1 = y - bottom
                if top is None:
                    y2 = y
                else:
                    if self.logy is True:
                        y2 = np.log10(10**y + top)
                    else:
                        y2 = y + top

            for i in range(len(x)):
                p.moveTo(x[i], y1[i])
                p.lineTo(x[i], y2[i])

            if beam is not None and beam > 0:
                x1 = x - beam/2.
                x2 = x + beam/2.
                if height is not None or top is not None:
                    for i in range(len(x)):
                        p.moveTo(x1[i], y2[i])
                        p.lineTo(x2[i], y2[i])
                if height is not None or bottom is not None:
                    for i in range(len(x)):
                        p.moveTo(x1[i], y1[i])
                        p.lineTo(x2[i], y1[i])

        width, right, left = self.opts['width'], \
                             self.opts['right'], \
                             self.opts['left']
        if width is not None or right is not None or left is not None:
            ## draw vertical error bars
            if width is not None:
                x1 = x - width/2.
                x2 = x + width/2.
            else:
                if left is None:
                    x1 = x
                else:
                    x1 = x - left
                if right is None:
                    x2 = x
                else:
                    x2 = x + right

            for i in range(len(x)):
                p.moveTo(x1[i], y[i])
                p.lineTo(x2[i], y[i])

            if beam is not None and beam > 0:
                y1 = y - beam/2.
                y2 = y + beam/2.
                if width is not None or right is not None:
                    for i in range(len(x)):
                        p.moveTo(x2[i], y1[i])
                        p.lineTo(x2[i], y2[i])
                if width is not None or left is not None:
                    for i in range(len(x)):
                        p.moveTo(x1[i], y1[i])
                        p.lineTo(x1[i], y2[i])

        self.path = p
        self.prepareGeometryChange()


#==============================================================================
class DevPlot(pg.PlotWidget):
    """The DevPlot class is dedicated to display a graph of a deviation serie.
    The class is derived from pyqtgraph.PlotWidget and customized for the
    specific needs of deviation serie display.
    """

    def __init__(self, parent=None, background='default', **kargs):
        """Constructor.
        """
        super().__init__(parent, background, **kargs)
        self.serie = None
        self.serie_unb = None
        self.asym = list()
        self.legend = None
        self._customize_plot()

    def _customize_plot(self):
        """Customize look of widget.
        :returns: None
        """
        self.setTitle('') # Cosmetic: add space above plot
        self.setLabel('left', "&sigma;<sub>y</sub> (&tau;)")
        self.setLabel('bottom', "Integration time &tau;", units='s')
        self.setLogMode(x=True, y=True)
        self.enableAutoRange(pg.ViewBox.XYAxes, False)
        self.showGrid(x=True, y=True, alpha=0.8)
        self.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
        self.getPlotItem().getAxis('left').enableAutoSIPrefix(False)
        #self.setClickable(False) ## Here?? ##

    def clear(self):
        super().clear()
        self.serie = None
        self.serie_unb = None
        self.asym = list()
        self.legend = None

    def plot_dev(self, x=[], y=[], yunb=None,
                 top1s=None, bottom1s=None,
                 top2s=None, bottom2s=None):
        """Plot deviation serie on graph.
        :returns: None
        """
        self.enableAutoRange(pg.ViewBox.XYAxes, True)
        if top1s is not None and bottom1s is not None:
            err = MyErrorBarItem(x=x, y=y, top=top1s, bottom=bottom1s,
                                 beam=0.03, pen=1.5)
            err.setLogMode(True, True)
            self.addItem(err)
        if top2s is not None and bottom2s is not None:
            err = MyErrorBarItem(x=x, y=y, top=top2s, bottom=bottom2s,
                                 beam=0.06, pen=0.4)
            err.setLogMode(True, True)
            self.addItem(err)
        if yunb is not None:
            self.serie_unb = self.plot(x, yunb, symbol='+',
                                       pen=None, name="serie_unb")
        self.serie = self.plot(x, y, symbol='x', pen=None, name="serie")
        self.legend = self.addLegend()
        self.enableAutoRange(pg.ViewBox.XYAxes, False)

    def plot_asymptotes(self, coeff, visible=False):
        x_range = [self.serie.xData[0], self.serie.xData[-1]]
        if coeff[0] != 0:
            ln = self._plot_formula(x_range, lambda x: np.sqrt(coeff[0] / x**3, 0))
            ln.opts['name'] = ('{' + LEGEND_FORMAT + '} &tau;<sup>-3/2</sup>').format(coeff[0])
            self.asym.append(ln)
        if coeff[1] != 0:
            ln = self._plot_formula(x_range, lambda x: np.sqrt(coeff[1] / x**2), 1)
            ln.opts['name'] = ('{' + LEGEND_FORMAT + '} &tau;<sup>-1</sup>'). \
                format(np.sqrt(coeff[1]))
            self.asym.append(ln)
        if coeff[2] != 0:
            ln = self._plot_formula(x_range, lambda x: np.sqrt(coeff[2] / x), 2)
            ln.opts['name'] = ('{' + LEGEND_FORMAT + '} &tau;<sup>-1/2</sup>'). \
                format(np.sqrt(coeff[2]))
            self.asym.append(ln)
        if coeff[3] != 0:
            ln = self._plot_formula(x_range, lambda x: np.sqrt(coeff[3] * x / x), 3)
            ln.opts['name'] = ('{' + LEGEND_FORMAT + '} &tau;<sup>0</sup>'). \
                format(np.sqrt(coeff[3]))
            self.asym.append(ln)
        if coeff[4] != 0:
            ln = self._plot_formula(x_range, lambda x: np.sqrt(coeff[4] * x), 4)
            ln.opts['name'] = ('{' + LEGEND_FORMAT + '} &tau;<sup>1/2</sup>'). \
                format(np.sqrt(coeff[4]))
            self.asym.append(ln)
        if coeff[5] != 0:
            ln = self._plot_formula(x_range, lambda x: np.sqrt(coeff[5] * x**2), 5)
            ln.opts['name'] = ('{' + LEGEND_FORMAT + '} &tau;<sup>1</sup>'). \
                format(np.sqrt(coeff[5]))
            self.asym.append(ln)
        self.set_asymptotes_visible(visible)

    def set_asymptotes_visible(self, visible=True):
        if visible is True:
            for line in self.asym:
                self.addItem(line)
        else:
            for line in self.asym:
                self.removeItem(line)
        del self.legend
        self.legend = self.addLegend()

    def _plot_formula(self, x_range, formula, order, nb_pts=10):
        """Plot a formula.
        :param formula: formula of curve to plot (lambda function)
        :param nb_pts: number of points to use to plot formula (int)
        :returns: None
        """
        x = np.logspace(np.log10(x_range[0]), np.log10(x_range[1]), nb_pts)
        return self.plot(x, formula(x), pen=(1, order*1.33))


#==============================================================================
class SerieWidget(QtWidgets.QWidget):
    """Widget dedicated to display the result of a serie computation and
    the parameters of the serie (deviance type, number of samples...).
    """

    def __init__(self):
        """Initialization.
        """
        super().__init__()
        self._setup_ui()
        self.preview_rbtn.released.connect(
            lambda: self.results.setCurrentWidget(self.preview))
        self.plot_rbtn.released.connect(
            lambda: self.results.setCurrentWidget(self.plot))
        self.table_rbtn.released.connect(
            lambda: self.results.setCurrentWidget(self.table))
        self.psd_rbtn.released.connect(
            lambda: self.results.setCurrentWidget(self.psd))
        self.params.param('Temporal analysis', 'Start').sigValueChanged.connect(self._xrange_changed)
        self.params.param('Temporal analysis', 'Stop').sigValueChanged.connect(self._xrange_changed)
        self.psd_rbtn.setVisible(False)
        self.psd.setVisible(False)
        self.preview_rbtn.setChecked(True)

    def _xrange_changed(self):
        start = self.params.param('Temporal analysis', 'Start').value()
        stop = self.params.param('Temporal analysis', 'Stop').value()
        self.params.param('Temporal analysis', 'Number of points').setValue(stop - start + 1)

    def _setup_ui(self):
        """Generate UI.
        :returns: None
        """
        select = QtWidgets.QHBoxLayout()
        self.preview_rbtn = QtWidgets.QRadioButton("Preview")
        self.plot_rbtn = QtWidgets.QRadioButton("Plot serie")
        self.table_rbtn = QtWidgets.QRadioButton("Table serie")
        self.psd_rbtn = QtWidgets.QRadioButton("PSD")
        select.addWidget(self.preview_rbtn)
        select.addWidget(self.plot_rbtn)
        select.addWidget(self.table_rbtn)
        select.addWidget(self.psd_rbtn)
        self.results = QtWidgets.QStackedLayout()
        self.preview = pg.PlotWidget()
        self.preview.setTitle('')  # In order to add space above plot
        self.preview.setDownsampling(auto=True, mode='peak')
        self.preview.enableAutoRange(pg.ViewBox.XYAxes, True)
        self.plot = DevPlot()
        self.table = pg.TableWidget(sortable=False)
        self.table.setFormat('%20.3e')  # Global formating
        self.table.setFormat('%29.1f', column=0)  # Specific time formating
        self.psd = pg.PlotWidget()
        self.psd.setTitle('')  # Cosmetic: add space above plot
        self.psd.setLabel('left', "PSD (dBHz^2/Hz)")
        self.psd.setLabel('bottom', "Frequency (Hz)")
        self.psd.enableAutoRange(pg.ViewBox.XYAxes, True)
        self.psd.getPlotItem().getAxis('bottom').enableAutoSIPrefix(False)
        self.psd.getPlotItem().getAxis('left').enableAutoSIPrefix(False)
        self.results.addWidget(self.preview)
        self.results.addWidget(self.plot)
        self.results.addWidget(self.table)
        self.results.addWidget(self.psd)
        self.params = Parameter.create(name='Serie parameters',
                                       type='group',
                                       children=param_dict)
        self.param_tree = ParameterTree()
        self.param_tree.setParameters(self.params, showTop=False)
        ui_layout = QtWidgets.QGridLayout()
        ui_layout.addWidget(self.param_tree, 0, 0, -1, 1)
        ui_layout.addLayout(select, 0, 1)
        ui_layout.addLayout(self.results, 1, 1)
        ui_layout.setColumnStretch(1, 2)
        self.setLayout(ui_layout)

    def set_data_preview(self, x, y):
        self.preview.clear()
        self.preview.plot(x, y, pen=(0, 0, 255))

    def set_data_serie(self, x, y, yunb=None, bmin1s=None, bmax1s=None,
                       bmin2s=None, bmax2s=None):
        """TODO handle table setData when data (bmin or bmax, yunb) are None 
        """
        if bmin1s is None or bmax1s is None:
            top1s = None
            bottom1s = None
        else:
            top1s = bmax1s - y
            bottom1s = y - bmin1s
        if bmin2s is None or bmax2s is None:
            top2s = None
            bottom2s = None
        else:
            top2s = bmax2s - y
            bottom2s = y - bmin2s
        self.plot.plot_dev(x=x, y=y, yunb=yunb,
                           top1s=top1s, bottom1s=bottom1s,
                           top2s=top2s, bottom2s=bottom2s)
        ##self.table.setData(np.transpose([x, y, yunb, bmin1s, bmax1s,
        ##                                 bmin2s, bmax2s]))
        self.table.setData(np.transpose([x, y,
                                         bmin1s, bmax1s,
                                         bmin2s, bmax2s]))
        self.table.setHorizontalHeaderLabels([u"\u03C4 (s)",
                                              u"\u03C3y",
                                              u"\u03C3y unbiased",
                                              u"Min \u03C3",
                                              u"Max \u03C3",
                                              u"Min 2\u03C3",
                                              u"Max 2\u03C3"])


#==============================================================================
class StMainWindow(QtWidgets.QMainWindow):
    """StMainWindow class, main UI of Sigma Theta program.
    """

    file_droped = QtCore.pyqtSignal(str)

    def __init__(self):
        """Constructor.
        """
        super().__init__()
        self.setWindowTitle(APP_NAME)
        # Lays out
        self._create_actions()
        self._menu_bar = self.menuBar()
        self._populate_menubar()
        self.tool_bar = self.addToolBar("Tool Bar")
        self._populate_toolbar()
        self.tool_bar.setMovable(True)
        self.tool_bar.setFloatable(False)
        self.status_bar = self.statusBar()
        self.serie_tab = QtWidgets.QTabWidget()
        self.serie_tab.addTab(SerieWidget(), "New")
        self.setCentralWidget(self.serie_tab)
        self.setAcceptDrops(True)
        # Init widget display
        self.reset()

    def _create_actions(self):
        """Creates actions used with bar widgets.
        :returns: None
        """
        self.action_import = QtWidgets.QAction(QtGui.QIcon.fromTheme("document-open"),
                                               "&Import", self)
        self.action_import.setStatusTip("Import text file")
        self.action_import.setShortcut('Ctrl+I')
        #
        self.action_new = QtWidgets.QAction(QtGui.QIcon.fromTheme("document-new"),
                                            "&New", self)
        self.action_new.setStatusTip("New serie analysis")
        self.action_new.setShortcut('Ctrl+N')
        #
        with impr.path(data, 'tch-32px.png') as p:
            self.action_new_tch = QtWidgets.QAction(QtGui.QIcon(str(p)),
                                                    "&Three cornered hat", self)
        self.action_new_tch.setStatusTip("New three cornered-hat analysis")
        self.action_new_tch.setShortcut('Ctrl+T')
        #
        """self.action_save = QAction(QIcon.fromTheme("document-save"),
                                   ("&Save"), self)
        self.action_save.setStatusTip("Save data")
        self.action_save.setShortcut('Ctrl+S')"""
        #
        self.action_quit = QtWidgets.QAction(QtGui.QIcon.fromTheme("application-exit"),
                                             "&Quit", self)
        self.action_quit.setStatusTip("Exit application")
        self.action_quit.setShortcut('Ctrl+Q')
        #
        self.action_run = QtWidgets.QAction(QtGui.QIcon.fromTheme("system-run"),
                                            "&Run", self)
        self.action_run.setStatusTip("Compute deviation serie")
        self.action_run.setShortcut('Ctrl+R')
        #
        """self.action_pref = QAction(QIcon.fromTheme("preferences-system"),
                                   "Preferences", self)
        self.action_pref.setStatusTip("Open preference dialog form")"""
        #
        with impr.path(data, 'x2y-32px.png') as p:
            self.action_x2y = QtWidgets.QAction(QtGui.QIcon(str(p)),
                                                "X2Y", self)
        self.action_x2y.setStatusTip("Phase to frequency conversion")
        #
        with impr.path(data, 'scale-32px.png') as p:
            self.action_scale = QtWidgets.QAction(QtGui.QIcon(str(p)),
                                                  "Scale", self)
        self.action_scale.setStatusTip("Scale data")
        #
        self.action_normalize = QtWidgets.QAction(QtGui.QIcon.fromTheme("go-last"),
                                                  "Normalize", self)
        self.action_normalize.setStatusTip("Normalize data")
        #
        with impr.path(data, 'dri-32px.png') as p:
            self.action_drirem = QtWidgets.QAction(QtGui.QIcon(str(p)),
                                                   "Drift remove", self)
        self.action_drirem.setStatusTip("Remove the linear drift")
        #
        with impr.path(data, 'psd-32px.png') as p:
            self.action_psd = QtWidgets.QAction(QtGui.QIcon(str(p)),
                                                "PSD", self)
        self.action_psd.setStatusTip("Frequency analysis")
        #
        self.action_about = QtWidgets.QAction(QtGui.QIcon.fromTheme("help-about"),
                                              "About {}".format(APP_NAME), self)
        self.action_about.setStatusTip("Open about dialog form")

    def _populate_menubar(self):
        """Populates the menubar of the UI
        :returns: None
        """
        self._menu_bar.menu_file = self._menu_bar.addMenu("&File")
        self._menu_bar.menu_file.addAction(self.action_import)
        self._menu_bar.menu_file.addSeparator()
        self._menu_bar.menu_file.addAction(self.action_new)
        ##self._menu_bar.menu_file.addAction(self.action_new_tch)
        self._menu_bar.menu_file.addSeparator()
        self._menu_bar.menu_file.addAction(self.action_quit)
        self._menu_bar.menu_process = self._menu_bar.addMenu("&Process")
        self._menu_bar.menu_process.addAction(self.action_run)
        self._menu_bar.menu_process.addAction(self.action_x2y)
        self._menu_bar.menu_process.addAction(self.action_normalize)
        self._menu_bar.menu_process.addAction(self.action_scale)
        self._menu_bar.menu_process.addAction(self.action_drirem)
        self._menu_bar.menu_process.addAction(self.action_psd)
        self._menu_bar.menu_help = self._menu_bar.addMenu("&Help")
        self._menu_bar.menu_help.addAction(self.action_about)

    def _populate_toolbar(self):
        """Populates the toolbar of the UI
        :returns: None
        """
        self.tool_bar.addAction(self.action_new)
        self.tool_bar.addAction(self.action_import)
        self.tool_bar.addAction(self.action_x2y)
        self.tool_bar.addAction(self.action_run)
        self.tool_bar.addAction(self.action_normalize)
        self.tool_bar.addAction(self.action_scale)
        self.tool_bar.addAction(self.action_drirem)
        self.tool_bar.addAction(self.action_psd)
        ##self.tool_bar.addAction(self.action_new_tch)

    @QtCore.pyqtSlot()
    def reset(self):
        """Reset UI. This is the starting state when no data file is imported,
        functions regarding data processing are not accessible.
        :returns: None
        """
        self.action_import.setEnabled(True)
        self.action_run.setEnabled(False)
        self.action_x2y.setEnabled(False)
        self.action_new.setEnabled(False)
        self.action_normalize.setEnabled(False)
        self.action_scale.setEnabled(False)
        self.action_drirem.setEnabled(False)
        self.action_psd.setEnabled(False)
        self.action_new_tch.setEnabled(False)

    @QtCore.pyqtSlot(bool)
    def computing_state(self, mode=True):
        """Set UI in computing mode. This the "normal" state, a data file is
        imported and all the functions need to be accessible.
        :returns: None
        """
        self.action_import.setEnabled(True)
        self.action_run.setEnabled(mode)
        self.action_x2y.setEnabled(mode)
        self.action_new.setEnabled(mode)
        self.action_normalize.setEnabled(mode)
        self.action_scale.setEnabled(mode)
        self.action_drirem.setEnabled(mode)
        self.action_psd.setEnabled(mode)
        self.action_new_tch.setEnabled(mode)

    def _setup_ui(self):
        """Generate UI.
        :returns: None

        !!?? TO REMOVE ??!!

        """
        select = QtWidgets.QHBoxLayout()
        self.preview_rbtn = QtWidgets.QRadioButton("Preview")
        self.plot_rbtn = QtWidgets.QRadioButton("Plot serie")
        self.table_rbtn = QtWidgets.QRadioButton("Table serie")
        select.addWidget(self.preview_rbtn)
        select.addWidget(self.plot_rbtn)
        select.addWidget(self.table_rbtn)
        self.results = QtWidgets.QStackedLayout()
        self.preview = pg.PlotWidget()
        self.preview.setTitle('')  # TODO: Display name of variance
        self.preview.setDownsampling(auto=True, mode='peak')
        self.preview.enableAutoRange(pg.ViewBox.XYAxes, True)
        self.plot = DevPlot()
        self.table = pg.TableWidget(sortable=False)
        self.table.setFormat('%25.3e')  # Global formating
        self.table.setFormat('%29.1f', column=0)  # Specific time formating
        self.results.addWidget(self.preview)
        self.results.addWidget(self.plot)
        self.results.addWidget(self.table)
        self.params = Parameter.create(name='Serie parameters',
                                      type='group',
                                      children=param_dict)
        self.param_tree = ParameterTree()
        self.param_tree.setParameters(self.params, showTop=False)
        ui_layout = QtWidgets.QGridLayout()
        ui_layout.addWidget(self.param_tree, 0, 0, -1, 1)
        ui_layout.addLayout(select, 0, 1)
        ui_layout.addLayout(self.results, 1, 1)
        ui_layout.setColumnStretch(1, 2)
        self.setLayout(ui_layout)

    def on_action_psd(self):
        """Set convenient UI for frequency analysis:
        - Add a specific items in parameter tree
        - Add a plot dedicated to display result of frequency analysis
        :returns: None
        """
        self.serie_tab.currentWidget().psd_rbtn.setVisible(True)
        self.serie_tab.currentWidget().psd_rbtn.setChecked(True)
        self.serie_tab.currentWidget().results.setCurrentWidget(
            self.serie_tab.currentWidget().psd)

    def on_action_x2y(self):
        """Open a parameters dialog box and get the choosen parameter
        values.
        :returns: scale factors (xscale, yscale) (float, float)
        """
        dialog = X2yDialog()
        dialog.setParent(None, Qt.Qt.Dialog)
        retval = dialog.exec_()
        if retval == QtWidgets.QDialog.Accepted:
            return dialog.xscale, dialog.yscale

    def on_action_scale(self):
        """Open a parameters dialog box and get the choosen parameter
        values.
        :returns: scale factors (offset, multiplier) (float, float)
        """
        dialog = ScalingDialog()
        dialog.setParent(None, Qt.Qt.Dialog)
        retval = dialog.exec_()
        if retval == QtWidgets.QDialog.Accepted:
            return dialog.scale, dialog.offset

    def on_action_drirem(self):
        """Open a parameters dialog box and get the choosen parameter
        values.
        :returns: Drift remove order (int)
        """
        dialog = DriRemDialog()
        dialog.setParent(None, Qt.Qt.Dialog)
        retval = dialog.exec_()
        if retval == QtWidgets.QDialog.Accepted:
            return dialog.drift_order

    def deviance_type(self):
        return self.serie_tab.currentWidget().params. \
            param('Temporal analysis', 'Deviance type').value()

    def tau_step(self):
        return self.serie_tab.currentWidget().params. \
            param('Temporal analysis', 'Tau step').value()

    def tau(self):
        return self.serie_tab.currentWidget().params. \
            param('Temporal analysis', 'Integration time (s)').value()

    def set_tau(self, value):
        self.serie_tab.currentWidget().params. \
            param('Temporal analysis', 'tau').setValue(value)

    def set_filename(self, value):
        self.serie_tab.currentWidget().params. \
            param('Temporal analysis', 'Filename'). \
            setValue(ntpath.basename(value))
        self.serie_tab.setTabText(self.serie_tab.currentIndex(), value)

    # The following three methods set up dragging and dropping for the app
    def dragEnterEvent(self, e):
        if e.mimeData().hasUrls:
            e.accept()
        else:
            e.ignore()

    def dragMoveEvent(self, e):
        if e.mimeData().hasUrls:
            e.accept()
        else:
            e.ignore()

    def dropEvent(self, e):
        """Drop files directly onto the widget.
        File locations are stored in fname.
        :param e:
        :return:
        """
        if e.mimeData().hasUrls:
            e.setDropAction(QtCore.Qt.CopyAction)
            e.accept()
            # Workaround for OSx dragging and dropping
            for url in e.mimeData().urls():
                fname = str(url.toLocalFile())
            self.file_droped.emit(fname)
        else:
            e.ignore()


#==============================================================================
if __name__ == "__main__":
    """Check UI display.
    """

    import sys
    import numpy as np
    from PyQt5.QtWidgets import QApplication


    # Handles log
    DATE_FMT = "%d/%m/%Y %H:%M:%S"
    LOG_FORMAT = "%(asctime)s %(levelname) -8s %(filename)s " + \
                 " %(funcName)s (%(lineno)d): %(message)s"
    logging.basicConfig(level=logging.INFO, \
                        datefmt=DATE_FMT, \
                        format=LOG_FORMAT)

    X = np.arange(1,10)
    Y = 2*np.arange(1,10)
    DY = 1*np.arange(1,10)
    DY2 = DY * 2
    COEFF = [0, 2.174317e-25, 9.832043e-26, 2.319278e-27, \
             1.501548e-30, 3.418445e-35]

    print(X, '\n', Y, '\n', DY)

    APP = QApplication(sys.argv)
    UI = StMainWindow()
    UI.serie_tab.currentWidget().set_data_serie(x=X, y=Y, bmin1s=DY, bmax1s=DY, bmin2s=DY2, bmax2s=DY2)
    #UI.serie_tab.currentWidget().plot.plot_asymptotes(COEFF, True)
    UI.show()
    sys.exit(APP.exec_())
