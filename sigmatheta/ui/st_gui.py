# -*- coding: utf-8 -*-

"""package sigmatheta
author    Benoit Dubois
copyright Benoit Dubois, 2022
email     dubois.benoit@gmail.com
licence   GPL3+
brief     Gui class file of SigmaTheta-gui program
"""

import os
import io
import logging
import pathlib
import math
import configparser
import numpy as np
from collections import OrderedDict
import pyqtgraph.parametertree as pgt
from PyQt5 import Qt
from PyQt5.QtCore import pyqtSignal, pyqtSlot, QObject, QDir, QFileInfo, \
    QSettings
from PyQt5.QtWidgets import QApplication, QFileDialog, QDialog, QMessageBox, \
    QGridLayout, QPushButton

import sigmatheta.ui.st_form as st_form
import sigmatheta.core.st_core as st_core
import st_binding.st_binding as stb
from sigmatheta.ui.csvimport import CsvImportDialog
from sigmatheta.version import __version__
from sigmatheta.constants import APP_NAME, APP_BRIEF, \
     AUTHOR_NAME, AUTHOR_MAIL, COPYRIGHT, LICENSE


# =============================================================================
def write_ini(obj, param, value):
    config = configparser.ConfigParser()
    config.read(os.getenv('NDTS_SERVER_INI'))
    config[obj][param] = str(value)
    with open(os.getenv('NDTS_SERVER_INI'), 'w') as configfile:
        config.write(configfile)

def read_ini(obj, param):
    retries_left = MAX_RETRY = 3
    config = configparser.ConfigParser()
    config.read(os.getenv('NDTS_SERVER_INI'))
    # Check is ini file is correctly read.
    # A bug (race condition?) conducts to read a void ini file.
    # -> simply try to re-read file multiple time.
    while config.has_section(obj) is False:
        logger.logging.error("Error when reading ini file, another try.")
        time.sleep(0.1)
        config.read(os.getenv('NDTS_SERVER_INI'))
        if retries_left > 0:
            retries_left -= 1
        else:
            raise Exception("Error when reading ini file")
    retval = config[obj][param]
    return retval

# =============================================================================
class StGui(QObject):

    dev_process_done = pyqtSignal(object)
    data_imported = pyqtSignal(object)

    def __init__(self):
        super().__init__()
        self.cdir = QDir.homePath()
        self.comment = '%'
        self.separator = '\t'
        self.data = {}
        self.ortau = stb.st_tau_inc()
        self.ui = st_form.StMainWindow()
        self.init_ui()
        self.actions_handling()
        self.logic_handling()
        self.ui.setVisible(True)

    def logic_handling(self):
        """Defines behaviour of logic.
        :returns: None
        """
        self.ui.file_droped.connect(self.import_data)
        self.data_imported.connect(lambda: self.ui.computing_state(True))
        self.data_imported.connect(self.update_file_tree_param)
        self.data_imported.connect(
            lambda: self.ui.serie_tab.currentWidget().set_data_preview(
            x=self.data[self.ui.serie_tab.currentIndex()][:, 0], 
            y=self.data[self.ui.serie_tab.currentIndex()][:, 1]))
        self.dev_process_done.connect(self.update_dev)
        self.ui.serie_tab.currentWidget().params. \
            param('Temporal analysis', 'Start'). \
            sigValueChanged.connect(self.update_preview)
        self.ui.serie_tab.currentWidget().params. \
            param('Temporal analysis', 'Stop'). \
            sigValueChanged.connect(self.update_preview)
        self.ui.serie_tab.currentWidget().params. \
            param('Temporal analysis', 'Display asymptote'). \
            sigValueChanged.connect(self.param_asymptotes_changed)

    def param_asymptotes_changed(self, param):
        self.ui.serie_tab.currentWidget().plot. \
            set_asymptotes_visible(param.value())

    def actions_handling(self):
        """Triggers ui actions: connects actions of ui with real actions.
        :param ui: UI containing acquisition configuration (QMainWindow)
        :param data:
        :returns: None
        """
        self.ui.action_import.triggered.connect(self.import_data)
        self.ui.action_x2y.triggered.connect(self.x2y)
        #ui.action_export.triggered.connect()
        #ui.action_save.triggered.connect()
        self.ui.action_quit.triggered.connect(self.ui.close)
        self.ui.action_new.triggered.connect(self.new_analysis)
        self.ui.action_new_tch.triggered.connect(self.new_tch)
        self.ui.action_run.triggered.connect(self.process_data)
        self.ui.action_scale.triggered.connect(self.scale)
        self.ui.action_normalize.triggered.connect(self.normalize)
        self.ui.action_drirem.triggered.connect(self.drirem)
        self.ui.action_psd.triggered.connect(self.psd)
        self.ui.action_about.triggered.connect(self.about)

    def init_ui(self):
        """Initialize UI.
        :returns: None
        """
        self.ui.setWindowTitle(APP_NAME)

    @pyqtSlot()
    def about(self):
        """Displays an about message box.
        :returns: None
        """
        QMessageBox.about(self.ui, "About " + APP_NAME,
                "<b>" + APP_NAME +  " " + __version__ + "</b><br>" +
                APP_BRIEF + ".<br>"
                "Author " + AUTHOR_NAME + ", " + AUTHOR_MAIL + " .<br>"
                "Copyright " + COPYRIGHT + ".<br>"
                "Licensed under the " + LICENSE)

    def path_leaf(path):
        """
        """
        head, tail = ntpath.split(path)
        return tail or ntpath.basename(head)

    @pyqtSlot()
    def import_data(self):
        """Get data from file.
        :returns: None
        """
        if not pathlib.Path(self.cdir).is_dir():
            self.cdir = QDir.homePath()
        dialog = CsvImportDialog(parent=self.ui,
                                 directory=self.cdir,
                                 comment=self.comment,
                                 separator=self.separator,
                                 encoding="latin_1")
        dialog.setWindowTitle("Import data")
        try:
            retval = dialog.exec()
        except Exception as ex:
            logging.warning("Problem when setting data file: %r", ex)
        if retval != QDialog.Accepted or dialog.filename == '':
            return
        #
        with open(dialog.filename, encoding=dialog.encoding) as fd:
            self.data[self.ui.serie_tab.currentIndex()] = np.genfromtxt(
                io.BytesIO(fd.read().encode('utf_8')),
                delimiter=dialog.separator,
                comments=dialog.comment,
                invalid_raise=False)
        #
        self.comment = dialog.comment
        self.separator = dialog.separator
        self.cdir = dialog.directory
        self.ui.set_filename(os.path.basename(
                             os.path.normpath(dialog.filename)))
        #
        self.data_imported.emit(self.data[self.ui.serie_tab.currentIndex()])

    @pyqtSlot()
    def process_data(self):
        """
        :returns: None
        """
        if self.ui.tau_step() == 2:
            ortau = stb.st_tau_inc((1, 2, 4), 3) # Decimal
        elif self.ui.tau_step() == 3:
            ortau = stb.st_tau_inc((1, 2, 3, 4, 5, 6, 7, 8, 9), 9) # All tau
        else:
            ortau = stb.st_tau_inc() # Octave
        tau = self.ui.serie_tab.currentWidget().params. \
            param('Temporal analysis', 'Integration time (s)').value()
        start = self.ui.serie_tab.currentWidget().params. \
            param('Temporal analysis', 'Start').value()
        stop = self.ui.serie_tab.currentWidget().params. \
            param('Temporal analysis', 'Stop').value()
        serie = st_core.process_dev(
            tau,
            self.data[self.ui.serie_tab.currentIndex()][start:stop, 1],
            self.ui.deviance_type(),
            ortau)
        self.dev_process_done.emit(serie)

    @pyqtSlot()
    def update_file_tree_param(self):
        size = len(self.data[self.ui.serie_tab.currentIndex()][:, 0])
        self.ui.serie_tab.currentWidget().params. \
            param('Temporal analysis', 'Start').setLimits((1, size))
        self.ui.serie_tab.currentWidget().params. \
            param('Temporal analysis', 'Stop').setLimits((1, size))
        self.ui.serie_tab.currentWidget().params.param('Temporal analysis', 'Start').setValue(1)
        self.ui.serie_tab.currentWidget().params.param('Temporal analysis', 'Stop').setValue(size)
        tau = (self.data[self.ui.serie_tab.currentIndex()][-1, 0] - self.data[self.ui.serie_tab.currentIndex()][0, 0]) / (size - 1)
        self.ui.serie_tab.currentWidget().params. \
            param('Temporal analysis', 'Integration time (s)').setValue(tau)

    @pyqtSlot(object)
    def update_dev(self, serie):
        # Basicaly x = serie.tau[:serie.length] but here we use integration time defined by user
        intt = self.ui.serie_tab.currentWidget().params. \
            param('Temporal analysis', 'Integration time (s)').value()
        c = intt / serie.tau[0]
        x = [t*c for t in serie.tau[:serie.length]]
        y = serie.dev[:serie.length]
        yunb = serie.dev_unb[:serie.length]
        bmin2s = np.fromiter((serie.conf_int[idx].bmin2s
                              for idx in range(serie.length)), np.float)
        bmax2s = np.fromiter((serie.conf_int[idx].bmax2s
                              for idx in range(serie.length)), np.float)
        bmin1s = np.fromiter((serie.conf_int[idx].bmin1s
                              for idx in range(serie.length)), np.float)
        bmax1s = np.fromiter((serie.conf_int[idx].bmax1s
                              for idx in range(serie.length)), np.float)
        self.ui.serie_tab.currentWidget().plot.clear()
        self.ui.serie_tab.currentWidget().table.clear()
        #
        self.ui.serie_tab.currentWidget().set_data_serie(x, y, yunb,
                                                         bmin1s, bmax1s,
                                                         bmin2s, bmax2s)
        self.ui.serie_tab.currentWidget().plot.plot_asymptotes(
            serie.asym,
            self.ui.serie_tab.currentWidget().params. \
                param('Temporal analysis', 'Display asymptote').value())

    @pyqtSlot()
    def update_preview(self):
        start = self.ui.serie_tab.currentWidget().params.param('Temporal analysis', 'Start').value()
        stop = self.ui.serie_tab.currentWidget().params.param('Temporal analysis', 'Stop').value()
        self.ui.serie_tab.currentWidget().set_data_preview(
            x=self.data[self.ui.serie_tab.currentIndex()][start:stop, 0],
            y=self.data[self.ui.serie_tab.currentIndex()][start:stop, 1])

    @pyqtSlot()
    def new_analysis(self):
        form = st_form.SerieWidget()
        form.params.param('Temporal analysis', 'Start'). \
            sigValueChanged.connect(self.update_preview)
        form.params.param('Temporal analysis', 'Stop'). \
            sigValueChanged.connect(self.update_preview)
        form.params.param('Temporal analysis', 'Display asymptote'). \
            sigValueChanged.connect(self.param_asymptotes_changed)
        self.ui.serie_tab.addTab(form, "New")        

    @pyqtSlot()
    def new_tch(self):
        pass

    @pyqtSlot()
    def scale(self):
        m_factor, s_factor = self.ui.on_action_scale()
        if m_factor is not None and s_factor is not None:
            self.data[self.ui.serie_tab.currentIndex()][:, 1] = st_core.scale(self.data[self.ui.serie_tab.currentIndex()][:, 1], m_factor, s_factor)
        self.update_preview()

    @pyqtSlot()
    def normalize(self):
        self.data[self.ui.serie_tab.currentIndex()][:, 1] = st_core.normalize(self.data[self.ui.serie_tab.currentIndex()][:, 1])
        self.update_preview()

    @pyqtSlot()
    def drirem(self):
        order = self.ui.on_action_drirem()
        start = self.ui.serie_tab.currentWidget().params.param('Temporal analysis', 'Start').value()
        stop = self.ui.serie_tab.currentWidget().params.param('Temporal analysis', 'Stop').value()
        self.data[self.ui.serie_tab.currentIndex()][start:stop, 1], coeffs = st_core.drift_remove(
            self.data[self.ui.serie_tab.currentIndex()][start:stop, 0],
            self.data[self.ui.serie_tab.currentIndex()][start:stop, 1],
            order)
        self.update_preview()
        #
        self.ui.serie_tab.currentWidget().params.addChild(
            pgt.Parameter.create(name='Drift coefficients',
                                 type='group',
                                 children=[]))
        for o in range(order+1):
            self.ui.serie_tab.currentWidget().params. \
                param('Drift coefficients').addChild(
                    pgt.Parameter.create(name=f"degree {o}",
                                         type="str",
                                         value=f"{coeffs[o]:+.5e}"))

    @pyqtSlot()
    def x2y(self):
        """
        """
        xscale, yscale = self.ui.on_action_x2y()
        start = self.ui.serie_tab.currentWidget().params.param('Temporal analysis', 'Start').value()
        stop = self.ui.serie_tab.currentWidget().params.param('Temporal analysis', 'Stop').value()
        if xscale is not None and yscale is not None:
            y = st_core.x2y(self.data[self.ui.serie_tab.currentIndex()][start:stop, 1], xscale, yscale)
            self.data[self.ui.serie_tab.currentIndex()] = np.array([self.data[self.ui.serie_tab.currentIndex()][start:stop-1, 0], y]).T
        self.update_preview()

    @pyqtSlot()
    def psd(self):
        """
        """
        self.ui.on_action_psd()
        tau = self.ui.serie_tab.currentWidget().params. \
                                 param('Temporal analysis',
                                       'Integration time (s)').value()
        N = self.ui.serie_tab.currentWidget().params. \
                                 param('Temporal analysis',
                                       'Number of points').value()
        start = self.ui.serie_tab.currentWidget().params.param('Temporal analysis', 'Start').value()
        stop = self.ui.serie_tab.currentWidget().params.param('Temporal analysis', 'Stop').value()
        nfft = 2**(math.floor(np.log2(N)))
        freq, psd = st_core.psd(self.data[self.ui.serie_tab.currentIndex()][start:stop, 1], 
                                fs=1/tau,
                                nfft=nfft)
        self.ui.serie_tab.currentWidget().psd.clear()
        self.ui.serie_tab.currentWidget().psd.plot(freq, np.log10(psd), pen=(0, 0, 255))   
        ##algo = self.ui.serie_tab.currentWidget().params.param('Frequency analysis', 'Algorithm').value()
