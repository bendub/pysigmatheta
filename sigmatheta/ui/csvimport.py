# -*- coding: utf-8 -*-

"""package csvimport
author    Benoit Dubois
copyright Benoit Dubois, 2022
email     dubois.benoit@gmail.com
licence   GPL3+
brief     TEXT/CSV import dialog gui.
"""

import io
import encodings
import logging
from functools import singledispatchmethod
import numpy as np
from PyQt5.QtWidgets import QDialog, QFileDialog, QGroupBox, QGridLayout, \
    QTableView, QPushButton, QLabel, QDialogButtonBox, QLineEdit, \
    QRadioButton, QMessageBox, QComboBox, QListView, QCheckBox, QButtonGroup
from PyQt5.QtCore import pyqtSlot, pyqtSignal, Qt, QDir, QAbstractTableModel, \
    QStringListModel
import pyqtgraph as pg

## Switch to using white background and black foreground
pg.setConfigOption('background', 'w')
pg.setConfigOption('foreground', 'k')

ENCODINGS = [e for e in sorted(set(encodings.aliases.aliases.values()))]

MAX_LINE = 5   # Number of lines displayed in the GUI.
MAX_PREVIEW_PTS = 1000
DEFAULT_COMMENT = '#'
DEFAULT_SEPARATOR = '\t'
DEFAULT_ENCODING = 'utf_8'


class MyQButtonGroup(QButtonGroup):
    """Add string identifier to QButtonGroup class.
    """
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.sid = {}  # "string id" dict

    def addButton(self, button, id=-1, sid=None):
        super().addButton(button, id)
        if sid is None or sid in self.sid.keys():
            # use (automaticaly) assigned id if sid not defined or already assigned
            self.sid[self.id(button)] = button
        else:
            self.sid[sid] = button

    def sid(self, button):
        """Returns the sid for the specified button, or -1 if no such button
        exists.
        """
        if button not in self.buttons():
            return -1
        return list(self.sid)[list(self.sid.values()).index(button)]

    @singledispatchmethod
    def button(self, idn):
        raise NotImplementedError("Bad usage of button() method.")

    @button.register
    def _(self, idn: int):
        return super().button(idn)

    @button.register
    def _(self, idn: str):
        return self.sid[idn]



class NumpyModel(QAbstractTableModel):

    def __init__(self, narray, parent=None):
        super().__init__(parent=parent)
        self._array = narray

    def rowCount(self, parent=None):
        return self._array.shape[0]

    def columnCount(self, parent=None):
        if len(self._array.shape) == 1:
            return 1
        return self._array.shape[1]

    def data(self, index, role=Qt.DisplayRole):
        if index.isValid():
            if role == Qt.DisplayRole:
                if self.columnCount() == 1:
                    row = index.row()
                    return str(self._array[row])
                row = index.row()
                col = index.column()
                return str(self._array[row, col])


class CsvImportDialog(QDialog):
    """A dialog widget dedicated to import text/csv type file.
    """

    comment_changed = pyqtSignal(str)
    separator_changed = pyqtSignal(str)

    def __init__(self, parent=None,
                 directory=QDir.homePath(),
                 comment=DEFAULT_COMMENT,
                 separator=DEFAULT_SEPARATOR,
                 encoding=DEFAULT_ENCODING,
                 xcol_enable=True):
        super().__init__(parent)
        if encoding not in ENCODINGS:
            raise ValueError("Illegal encoding parameter")
        self.directory = directory
        self.encoding = encoding
        self._xcol_enable = xcol_enable
        self.timetag_column = 0
        self.data_column = 1
        self.filename = ''
        self.preview_graph = None
        self.comment = comment
        self.separator = separator
        self._setup_ui()
        #
        self._encoding_cbox.setCurrentIndex(
            self._encoding_cbox.findText(encoding))
        self.comment_btns.button(comment).setChecked(True)
        self.separator_btns.button(separator).setChecked(True)
        #
        self._open_btn.released.connect(self._open_triggered)
        self._encoding_cbox.currentIndexChanged[str].connect(self._set_encoding)
        #
        self._sharp_rbtn.toggled.connect(lambda: self._set_comment('#'))
        self._percent_rbtn.toggled.connect(lambda: self._set_comment('%'))
        self._comment_other_rbtn.toggled.connect(
            lambda: self._set_comment(self._comment_other_led.text()))
        self._comment_other_led.textChanged.connect(
            lambda: self._set_comment(self._comment_other_led.text()))
        self._comment_other_rbtn.toggled.connect(
            self._comment_other_led.setEnabled)
        #
        self._tab_rbtn.toggled.connect(lambda: self._set_separator('\t'))
        self._comma_rbtn.toggled.connect(lambda: self._set_separator(','))
        self._semicolon_rbtn.toggled.connect(lambda: self._set_separator(';'))
        self._space_rbtn.toggled.connect(lambda: self._set_separator(' '))
        self._sep_other_rbtn.toggled.connect(
            lambda: self._set_separator(self._sep_other_led.text()))
        self._sep_other_led.textChanged.connect(
            lambda: self._set_separator(self._sep_other_led.text()))
        self._sep_other_rbtn.toggled.connect(self._sep_other_led.setEnabled)
        #
        if self._xcol_enable is True:
            self._column_t_cbox.currentIndexChanged[str].connect(self._set_t_column)
        self._column_y_cbox.currentIndexChanged[str].connect(self._set_y_column)
        #
        self.separator_changed.connect(self._update)
        self.comment_changed.connect(self._update)
        #
        self._preview_ckbox.stateChanged.connect(self._update_preview)
        self._column_t_cbox.currentIndexChanged.connect(self._update_preview)
        self._column_y_cbox.currentIndexChanged.connect(self._update_preview)
        self.separator_changed.connect(self._update_preview)
        self.comment_changed.connect(self._update_preview)
        #
        self._close_btns.button(QDialogButtonBox.Ok).clicked.connect(
            self.accept)
        self._close_btns.button(QDialogButtonBox.Cancel).clicked.connect(
            self.reject)
        #
        self._update()

    def _setup_ui(self):
        self.setWindowTitle("Text file import")
        #
        grid_layout = QGridLayout()
        grid_layout.setContentsMargins(11, 11, 11, 11)
        grid_layout.setSpacing(3)
        #
        open_gbox = QGroupBox("Import")
        open_layout = QGridLayout()
        self._open_btn = QPushButton("Open")
        self.file_lbl = QLabel(self.filename)
        self._encoding_cbox = QComboBox()
        self._encoding_cbox.addItems(ENCODINGS)
        open_layout.addWidget(self._open_btn, 0, 0)
        open_layout.addWidget(self.file_lbl, 0, 1, 1, 2)
        open_layout.addWidget(QLabel("Character encoding"), 1, 0)
        open_layout.addWidget(self._encoding_cbox, 2, 0)
        open_layout.setColumnStretch(2, 1)
        open_gbox.setLayout(open_layout)
        #
        comment_gbox = QGroupBox("Comment character")
        comment_layout = QGridLayout()
        self.comment_btns = MyQButtonGroup()
        self._sharp_rbtn = QRadioButton("#")
        self._percent_rbtn = QRadioButton("%")
        self._comment_other_rbtn = QRadioButton("Other")
        self._comment_other_led = QLineEdit()
        self.comment_btns.addButton(self._sharp_rbtn, sid="#")
        self.comment_btns.addButton(self._percent_rbtn, sid="%")
        self.comment_btns.addButton(self._comment_other_rbtn, sid="Other")
        comment_layout.addWidget(self._sharp_rbtn, 0, 0)
        comment_layout.addWidget(self._percent_rbtn, 0, 1)
        comment_layout.addWidget(self._comment_other_rbtn, 0, 2)
        comment_layout.addWidget(self._comment_other_led, 0, 3)
        comment_gbox.setLayout(comment_layout)
        #
        sep_gbox = QGroupBox("Separator")
        sep_layout = QGridLayout()
        self.separator_btns = MyQButtonGroup()
        self._tab_rbtn = QRadioButton("Tab")
        self._comma_rbtn = QRadioButton("Comma")
        self._semicolon_rbtn = QRadioButton("Semicolon")
        self._space_rbtn = QRadioButton("Space")
        self._sep_other_rbtn = QRadioButton("Other")
        self.separator_btns.addButton(self._tab_rbtn, sid="\t")
        self.separator_btns.addButton(self._comma_rbtn, sid=",")
        self.separator_btns.addButton(self._semicolon_rbtn, sid=";")
        self.separator_btns.addButton(self._space_rbtn, sid=" ")
        self.separator_btns.addButton(self._sep_other_rbtn, sid="Other")
        self._sep_other_led = QLineEdit()
        sep_layout.addWidget(self._tab_rbtn, 0, 0)
        sep_layout.addWidget(self._comma_rbtn, 0, 1)
        sep_layout.addWidget(self._semicolon_rbtn, 0, 2)
        sep_layout.addWidget(self._space_rbtn, 0, 3)
        sep_layout.addWidget(self._sep_other_rbtn, 0, 4)
        sep_layout.addWidget(self._sep_other_led, 0, 5)
        sep_gbox.setLayout(sep_layout)
        #
        column_gbox = QGroupBox("Choose columns")
        column_layout = QGridLayout()
        self._column_t_cbox = QComboBox()
        self._column_y_cbox = QComboBox()
        tlabel = QLabel("Timetag")
        column_layout.addWidget(tlabel, 0, 0)
        column_layout.addWidget(self._column_t_cbox, 0, 1)
        column_layout.addWidget(QLabel("Data"), 0, 2)
        column_layout.addWidget(self._column_y_cbox, 0, 3)
        if self._xcol_enable is False:
            tlabel.setEnabled(False)
            self._column_t_cbox.setEnabled(False)
        column_layout.setColumnStretch(1, 1)
        column_layout.setColumnStretch(3, 1)
        column_gbox.setLayout(column_layout)
        #
        rawview_gbox = QGroupBox("Raw view")
        self._file_view = QListView()
        rawview_layout = QGridLayout()
        rawview_layout.addWidget(self._file_view, 0, 0)
        rawview_gbox.setLayout(rawview_layout)
        #
        format_gbox = QGroupBox("Formated view")
        self._preview_ckbox = QCheckBox("Graphical preview")
        self._format_view = QTableView()
        self._preview_graph = pg.PlotWidget()
        self._preview_graph.getPlotItem().showAxes(True, False)
        format_layout = QGridLayout()
        format_layout.addWidget(self._preview_ckbox, 0, 0)
        format_layout.addWidget(self._format_view, 1, 0)
        format_layout.addWidget(self._preview_graph, 1, 1)
        format_layout.setColumnStretch(0, 2)
        format_layout.setColumnStretch(1, 2)
        format_gbox.setLayout(format_layout)
        #
        self._close_btns = QDialogButtonBox(QDialogButtonBox.Ok|
                                            QDialogButtonBox.Cancel)
        #
        row = 0
        grid_layout.addWidget(open_gbox, row, 0, 1, 2)
        row +=1
        grid_layout.addWidget(rawview_gbox, row, 0, 1, 2)
        row +=1
        grid_layout.addWidget(comment_gbox, row, 0)
        grid_layout.addWidget(sep_gbox, row, 1, )
        row +=1
        grid_layout.addWidget(column_gbox, row, 0, 1, 2)
        row +=1
        grid_layout.addWidget(format_gbox, row, 0, 1, 2)
        row +=1
        grid_layout.addWidget(self._close_btns, row, 0, 1, 2)
        grid_layout.setColumnStretch(1, 3)
        self.setLayout(grid_layout)

    def _set_t_column(self, t):
        self.timetag_column = t

    def _set_y_column(self, y):
        self.data_column = y

    def _set_encoding(self, encoding):
        self.encoding = encoding

    def _set_comment(self, comment):
        self.comment = comment
        self.comment_changed.emit(comment)

    def _set_separator(self, separator):
        self.separator = separator
        self.separator_changed.emit(separator)

    def _set_filename(self, filename):
        self.filename = filename
        self.file_lbl.setText(filename)

    @pyqtSlot()
    def _update_preview(self):
        if self.filename == '':
            return
        if self._preview_ckbox.isChecked() is True:
            self._preview_graph.clear()
            self.plot_preview()
        else:
            self._preview_graph.clear()

    def plot_preview(self):
        # Read file
        _encoding = self._encoding_cbox.currentText()
        try:
            # Problem with open()??
            #with open(self.filename, encoding=_encoding) as fd:
            data = np.genfromtxt(self.filename,
                                 delimiter=self.separator,
                                 comments=self.comment,
                                 encoding=_encoding)
            data = np.transpose(data)
        except TypeError:
            logging.warning("Data type error")
            return
        except UnicodeDecodeError:
            logging.warning("Bad character encoding selected, choose another one")
            return
        except ValueError as ex:
            logging.warning("Parsing file problem")
            logging.debug("Parsing file problem: %r", ex)
            return
        # Use only y data for displaying preview.
        data = np.array(data[int(self._column_y_cbox.currentText())-1])
        if len(data) > MAX_PREVIEW_PTS:
            ds = int( len(data) / MAX_PREVIEW_PTS )
            self._preview_graph.setDownsampling(ds=ds, mode='peak')
        self._preview_graph.plot(data)

    @pyqtSlot()
    def _open_triggered(self):
        """
        """
        filename, filter = QFileDialog.getOpenFileName(self,
                                                       "Import texte file",
                                                       self.directory)
        if filename != '':
            self._set_filename(filename)
            self._update()

    @pyqtSlot()
    def _update(self):
        if self.filename == '':
            return False
        # Read file
        _encoding = self._encoding_cbox.currentText()
        try:
            with open(self.filename, encoding=_encoding) as fd:
                i = 0
                lines = ''
                # Read MAX_LINE lines of data (not lines of comments!)
                while i < MAX_LINE:
                    line = fd.readline()
                    if line[0] != self.comment:
                        lines += line
                        i += 1
        except UnicodeDecodeError:
            QMessageBox.warning(self, "Encoding problem",
                                "Bad character encoding selected, choose another one")
            return False
        # Parse lines
        try:
            data = np.genfromtxt(io.BytesIO(lines.encode()),
                                 delimiter=self.separator,
                                 comments=self.comment)
        except ValueError as ex:
            logging.warning("Parsing file problem: %s", ex)
            return False
        # Update raw view
        rmodel = QStringListModel([line for line in lines.split('\n')])
        self._file_view.setModel(rmodel)
        # Update formated (data) view
        fmodel = NumpyModel(data)
        self._format_view.setModel(fmodel)
        #
        self._column_t_cbox.clear()
        self._column_y_cbox.clear()
        try:
            self._column_t_cbox.addItem(" ")
            for col in range(fmodel.columnCount()):
                self._column_t_cbox.addItem(str(col+1))
                self._column_y_cbox.addItem(str(col+1))
        except IndexError as ex:
            logging.debug(ex)
        if fmodel.columnCount() == 1:
            self._column_t_cbox.setCurrentIndex(0)
            self._column_y_cbox.setCurrentIndex(0)
        else:
            self._column_t_cbox.setCurrentIndex(1)
            self._column_y_cbox.setCurrentIndex(1)
        return True

#--------------------------------------------------------------------------
if __name__ == '__main__':
    import sys
    from PyQt5.QtWidgets import QApplication

    # Handles log
    DATE_FMT = "%d/%m/%Y %H:%M:%S"
    LOG_FORMAT = "%(asctime)s %(levelname) -8s %(filename)s " + \
                 " %(funcName)s (%(lineno)d): %(message)s"
    logging.basicConfig(level=logging.DEBUG, \
                        datefmt=DATE_FMT, \
                        format=LOG_FORMAT)

    APP = QApplication(sys.argv)
    DIALOG = CsvImportDialog(directory="../",
                             encoding="latin_1",
                             xcol_enable=True)
    DIALOG.show()
    sys.exit(APP.exec_())
