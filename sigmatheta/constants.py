# sigmatheta/constants.py

APP_NAME = "SigmaTheta-Gui"
APP_BRIEF = "GUI for Sigma Theta library."
AUTHOR_NAME = "Benoit Dubois"
AUTHOR_MAIL = "dubois.benoit@gmail.com"
COPYRIGHT = "Benoit Dubois, 2022"
LICENSE = "GNU GPL v3.0 or upper."
