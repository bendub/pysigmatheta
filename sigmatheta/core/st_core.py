# -*- coding: utf-8 -*-

"""package sigmatheta
author    Benoit Dubois
copyright Benoit Dubois, 2022
email     dubois.benoit@gmail.com
licence   GPL3+
brief     Data processing functions of Sigma Theta package.
"""

import logging
import st_binding.st_binding as stb
import scipy.signal as scs
import numpy as np
import numpy.polynomial.polynomial as poly


SCALE = {'d': 86400,
         'H': 3600,
         'M': 60,
         'm': 1.e-3,
         'u': 1.e-6,
         'n': 1.e-9,
         'p': 1.e-12}

DEV_SLOPE = {
    'ADEV': np.array([-2, -1, 0, 1, 2]),
    'MDEV': np.array([-3, -2, -1, 0, 1, 2]),
    'HDEV': np.array([-1, 0, 1, 2, 3]),
    'PDEV': np.array([-3, -2, -1, 0, 1, 2])
}

FLAG_SLOPE = {
    'ADEV': np.array([0, 1, 1, 1, 1, 1]),
    'MDEV': np.array([1, 1, 1, 1, 1, 1]),
    'HDEV': np.array([0, 0, 1, 1, 1, 1]),
    'PDEV': np.array([1, 1, 1, 1, 1, 1])
}

# =============================================================================
def process_dev(tau, y, deviation_type, ortau=stb.st_tau_inc()):
    """Process deviation: compute frequency instability.
    :param tau: Integration time in second (int)
    :param y: a 1D array containing temporal data (numpy.array)
    :param deviation_type: index (see st_binding.py) representing deviation
                           type to use (int)
    :param ortau: Integration time increment structure (object)
    :returns: result of deviation processing (object)
    """
    flag_bias = 0       # Unbiased estimate flag
    flag_variance = 0
    flag_slopes = FLAG_SLOPE[deviation_type]
    #
    n = len(y)
    serie = stb.st_serie()
    edf = np.empty([128,], dtype=float)
    wght = np.empty([128,], dtype=float)
    #
    try:
        stb.st_serie_dev(serie, ortau, stb.DEVIANCE_TYPE[deviation_type],
                         n, tau, y)
    except Exception as ex:
        logging.error("Exception: %r", ex)
        raise ex
    #
    if False: # If "edf file exist"
        logging.info("# Asymptote computed respectively from weighted data")
        if ortau.length > 2:
            ineq = 0
            for i in range(ortau.length):
                if (serie.tau[i]-to2[i]) / serie.tau[i] > 1e-3:
                    ineq += 1
        if (ortau.length != serie.length) or (ortau.length <= 0) or (ineq > 0):
            for i in range(serie.length):
                wght[i] = serie.tau[i]
        else:
            for i in range(serie.length):
                wght[i] = 1 / edf[i]
    else: # If "No edf file"
        for i in range(serie.length):
            wght[i] = serie.tau[i] # init tmp
    #
    stb.st_relatfit(serie, wght, 6, flag_slopes)
    #
    stb.st_asym2alpha(serie, flag_variance)
    #
    stb.st_avardof(serie, edf, flag_variance)
    #
    stb.st_aduncert(serie, edf, flag_variance)
    #
    return serie


# =============================================================================
def drift_remove(t_data, f_data, order, filter_=False):
    """Remove drift: output_data = data - polynomial_fit_of_data
    Compute the least-squares fit of a polynomial to input data then supress
    the polynomial to these data.
    :param t_data: a 1D array of timetag serie (numpy.array)
    :param f_data: a 1D array of frequency serie  (numpy.array)
    :param order: order of the polynomial (int)
    :param filter: flag handling post-process filtering of data (bool)
    :returns: "de-drifted" data, fit coefficients (numpy.array, numpy.array)
    """
    # Suppress mean time to get better fit:
    t_data = t_data - t_data.mean()
    try:
        coeffs = poly.polyfit(t_data, f_data, order)
    except Exception as ex:
        logging.error("Exception: %r", ex)
        raise ex
    yfit = poly.polyval(t_data, coeffs)
    f_data = f_data - yfit
    if filter_ is True:
        b, a = scs.butter(3, 0.005)
        ##res = scs.filtfilt(b, a, f_data, padlen=150)
    return f_data, coeffs


# =============================================================================
def normalize(data):
    """Normalize data: output_data = data - mean(data)
    :param data: a 1D array of data to normalize (numpy.array)
    :returns: data normalized (numpy.array)
    """
    return data - np.mean(data)


# =============================================================================
def scale(data, m_factor=1.0, s_factor=0.0):
    """Scale data: output_data = data * m_factor + s_factor
    :param data: a 1D array of data to scale (numpy.array)
    :param m_factor: multiplier factor (float)
    :param s_factor: sum factor (float)
    :returns: data scaled
    """
    return data * m_factor + s_factor


# =============================================================================
def x2y(xdata, tau=1, xscale=1):
    """Convert phase serie to frequency fluctuation serie.
    :param xdata: a 1D array of phase value (numpy.array)
    :returns: data converted to frequency fluctuation (numpy.array)
    """
    ydata = np.diff(xdata) / tau / xscale
    return ydata


# =============================================================================
def psd(data, fs, nfft):
    freq, pxx = scs.welch(x=data, fs=fs, nfft=nfft, scaling='density')
    return freq, pxx


# =============================================================================
def process_tch(sab, sbc, sca):
    """Computes individual instability of 3 sources A, B and C using the Three
    Cornered Hat algorithm.
    :param sab: deviation serie of sources A and B (array)
    :param sbc: deviation serie of sources B and C (array)
    :param sca: deviation serie of sources C and A (array)
    :returns: individual deviation serie of sources A, B and C (array, array, array)
    """
    #TODO: check behavior of np.sqrt(x) when x < 0 (result is a complex)
    sa = np.sqrt(np.pow(sab, 2) + np.pow(sca, 2) - np.pow(sbc, 2))
    sb = np.sqrt(np.pow(sbc, 2) + np.pow(sab, 2) - np.pow(sca, 2))
    sc = np.sqrt(np.pow(sca, 2) + np.pow(sbc, 2) - np.pow(sab, 2))
    return sa, sb, sc
